#include "StateTree.h"
#include <assert.h>
#include <time.h>
#include <queue>
#include <vector>
#include <iostream>

StateTree::StateTree(Color color):root_(std::make_unique<Node>(ReversiState(), 0)), color_(color)
{
}

void StateTree::Initialize(Color color)
{
    root_.release();
    root_ = std::make_unique<Node>(ReversiState(), 0);
    root_->Parent = nullptr;
    this->color_ = color;
}

void StateTree::ProceedWithMove(Move move)
{
    if(!(root_->Expanded))
        root_->Expand();
    auto next = root_->Passes.find(move);
    assert(next != root_->Passes.end());
    root_ = std::move(next->second);
    if(!(root_->Expanded))
        root_->Expand();
    if(root_->Passes.size() == 0){
        root_->State.SwapTurn();
        root_->Expand();
    }
    root_->Parent = nullptr;
}

Move StateTree::GetMove(double timer)
{
    if(!root_->Expanded)
        root_->Expand();
    clock_t startTime = clock();
    std::queue<Node*> toExpand;
    std::vector<Node*> updateNodes;
    for(auto p = root_->Passes.begin(); p != root_->Passes.end(); ++p)
            toExpand.push(p->second.get());
    auto chosenNode = root_->Passes.begin();
    int depth = chosenNode->second->Depth;
    int oldDepth = depth;
    while (toExpand.size() > 0) {
        //std::cout << (double)(clock() - startTime) / CLOCKS_PER_SEC << timer << std::endl;
        if((double)((clock() - startTime)) / CLOCKS_PER_SEC > timer - 1.0)
            break;
        auto node = toExpand.front();
        depth = node->Depth;
        if(depth != oldDepth){
            this->UpdateScores(updateNodes);
        }
        if((double)((clock() - startTime)) / CLOCKS_PER_SEC > timer - 1.0)
            break;
        updateNodes.push_back(node);
        toExpand.pop();
        //std::cout << updateNodes.size() << " ";
        if(node->Expanded){
            for(auto p = node->Passes.begin(); p != node->Passes.end(); ++p)
                    toExpand.push(p->second.get());
            continue;
        }
        node->Expand();
        if(node->Passes.size() == 0) {
            node->State.SwapTurn();
            node->Expand();
        }
        if(node->Passes.size() == 0)
            continue;
        for(auto p = node->Passes.begin(); p != node->Passes.end(); ++p)
        {
                toExpand.push(p->second.get());
                p->second->Score = p->second->State.Get(root_->State.GetTurn());
        }
        oldDepth = depth;
    }
    for(auto p = root_->Passes.begin(); p != root_->Passes.end(); ++p)
        if(p->second->Score > chosenNode->second->Score)
            chosenNode = p;
    auto move = chosenNode->first;
    this->ProceedWithMove(chosenNode->first);
    return move;
}

void StateTree::UpdateScores(std::vector<Node*>& updateNodes)
{
    for(long i = updateNodes.size() - 1; i >=0; --i)
    {
        auto node = updateNodes[i];
        if(node->Passes.size() == 0)
            continue;
        auto best = node->Passes.begin()->second.get();
        double highScore = node->State.GetTurn() == root_->State.GetTurn() ? 0 : 1000;
        for(auto p = node->Passes.begin(); p != node->Passes.end(); ++p)
        {
                if(node->State.GetTurn() == root_->State.GetTurn()){
                    if(p->second->Score > highScore){
                        highScore = p->second->Score;
                        best = p->second.get();
                    }
                }
                else{
                    if(p->second->Score < highScore){
                        highScore = p->second->Score;
                        best = p->second.get();
                    }
                }
        }
        node->Score = best->Score;
    }
}

void StateTree::OpponentMove(Move move)
{
	this->ProceedWithMove(move);
}

void StateTree::Node::Expand()
{
    this->Expanded = true;
    auto moves = this->State.GetPossibleMoves();
    for(auto move: moves)
        Passes.insert(std::pair<Move, std::unique_ptr<Node>>(move.first,  std::unique_ptr<Node>(new Node(move.second, this->Depth + 1, this))));
}
