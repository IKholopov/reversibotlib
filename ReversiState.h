#ifndef REVERSISTATE_H
#define REVERSISTATE_H

#include <utility>
#include <vector>

enum Color {Black = 1, White = 0};
enum Region {Center = 0, SubEdge = 1, Edge = 2, SubCorner = 3, Corner = 4};

struct Move{
        char X;
        int Y;
        Region GetRegion()
        {
            if(X - 'a' >= 2 && X - 'a' < 6 && Y >= 2 && Y < 6 )
                return Center;
            if((X - 'a' >= 2 && X - 'a' < 6 && (Y == 1 || Y == 6 )) ||
                    ((X - 'a' == 1 || X - 'a' == 6) && Y >= 2 && Y < 6 ))
                return SubEdge;
            if((X - 'a' >= 2 && X - 'a' < 6 && (Y == 0 || Y == 7 )) ||
                    ((X - 'a' == 0 || X - 'a' == 7) && Y >= 2 && Y < 6 ))
                return Edge;
            if((X - 'a' == 0 && Y == 0 ) || (X - 'a' == 7 && Y == 0 ) ||
               (X - 'a' == 0 && Y == 7 ) || (X - 'a' == 7 && Y == 7 ) )
                return Corner;
            return SubCorner;
        }
        bool operator==(const Move &other) const
          { return (X == other.X
                    && Y == other.Y);
          }
};

class ReversiState
{
    public:
        ReversiState();
        ReversiState(ReversiState& state, Color turn, Move move);
        void ApplyMove(Move move);
        int Get(Color color);
        Color GetTurn();
        void SwapTurn();
        std::vector<std::pair<Move, ReversiState> > GetPossibleMoves();
        int state_[8][8];
        int potentials_[8][8];
    private:
        Color turn_;
};


namespace std {

    template<class T>
    struct hash;
    template<>
    struct hash<Move>
      {
        std::size_t operator()(const Move& k) const
        {
          using std::size_t;
          using std::hash;
          using std::string;

          return hash<char>()(k.X)
                   ^ (hash<int>()(k.Y) << 1);
        }
      };

}
#endif // REVERSISTATE_H
