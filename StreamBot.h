#ifndef STREAMBOT_H
#define STREAMBOT_H

#include "IBot.h"
#include "memory"
#include "istream"
#include "ostream"

class StreamBot
{
    public:
        StreamBot(std::unique_ptr<IBot> bot, std::istream& input, std::ostream& output,
                  double timer = 3);

        void Run();
    private:
        std::unique_ptr<IBot> bot_;
        std::istream& input_;
        std::ostream& output_;
        double timer_;
};

#endif // STREAMBOT_H
