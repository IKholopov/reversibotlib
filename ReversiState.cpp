#include "ReversiState.h"

#include "Logger.h"
#include <iostream>
#include <sstream>


ReversiState::ReversiState():turn_(Black)
{
    for(int i = 0;  i < 8; ++i)
        for(int j = 0; j < 8; ++j)
            state_[i][j] = -1;
    state_[3][4] = Black;
    state_[3][3] = White;
    state_[4][4] = White;
    state_[4][3] = Black;
    int potentials[8][8] = {{99, -8, 8, 6, 6, 8, -8, 99},
                  {-8, -24, -4, -3, -3, -4, -24, -8},
                  {8, -4, 7, 4, 4, 7, -4, 8},
                  {6, -3, 4, 0, 0, 4, -3, 6},
                  {6, -3, 4, 0, 0, 4, -3, 6},
                  {8, -4, 7, 4, 4, 7, -4, 8},
                  {-8, -24, -4, -3, -3, -4, -24, -8},
                  {99, - 8, 8, 6, 6, 8, -8, 99}};
    for(int i = 0;  i < 8; ++i)
        for(int j = 0; j < 8; ++j)
            potentials_[i][j] = potentials[i][j];
}

ReversiState::ReversiState(ReversiState& state, Color turn, Move move)
{
    for(int i = 0;  i < 8; ++i)
        for(int j = 0; j < 8; ++j)
            state_[i][j] = state.state_[i][j];
    for(int i = 0;  i < 8; ++i)
        for(int j = 0; j < 8; ++j)
            potentials_[i][j] = state.potentials_[i][j];
    if(move.GetRegion() == Corner || move.GetRegion() == SubCorner){
        int x = move.X - 'a';
        int y = move.Y;
        if(x - 1 >= 0)
            potentials_[x - 1][y] += potentials_[x][y] / 2;
        if(x + 1 < 8)
            potentials_[x + 1][y] += potentials_[x][y] / 2;
        if(y - 1 >= 0)
            potentials_[x][y - 1] += potentials_[x][y] / 2;
        if(y + 1 < 8)
            potentials_[x][y + 1] += potentials_[x][y] / 2;
    }

    this->turn_ = turn;
    this->ApplyMove(move);
}

void ReversiState::ApplyMove(Move move)
{
    state_[move.X - 'a'][move.Y] = turn_;
    for(int i = move.X - 'a' - 1; i >= 0; --i)
    {
        if(state_[i][move.Y] == turn_) {
            for(int j = move.X - 'a' - 1; j > i; --j)
                state_[j][move.Y] = turn_;
            break;
        }
        if(state_[i][move.Y] == -1)
            break;
    }
    for(int i = move.X - 'a' + 1; i < 8; ++i)
    {
        if(state_[i][move.Y] == turn_) {
            for(int j = move.X - 'a' + 1; j < i; ++j)
                state_[j][move.Y] = turn_;
            break;
        }
        if(state_[i][move.Y] == -1)
            break;
    }
    for(int i = move.Y - 1; i >= 0; --i)
    {
        if(state_[move.X - 'a'][i] == turn_) {
            for(int j = move.Y - 1; j > i; --j)
                state_[move.X - 'a'][j] = turn_;
            break;
        }
        if(state_[move.X - 'a'][i] == -1)
            break;
    }
    for(int i = move.Y + 1; i < 8; ++i)
    {
        if(state_[move.X - 'a'][i] == turn_) {
            for(int j = move.Y + 1; j < i; ++j)
                state_[move.X - 'a'][j] = turn_;
            break;
        }
        if(state_[move.X - 'a'][i] == -1)
            break;
    }
    for(int i = 1; move.X  - 'a' - i >= 0 && move.Y - i >= 0; ++i)
    {
        if(state_[move.X - 'a' - i][move.Y - i] == turn_) {
            for(int j = 1; j < i; ++j)
                state_[move.X - 'a' - j][move.Y - j] = turn_;
            break;
        }
        if(state_[move.X - 'a' - i][move.Y - i] == -1)
            break;
    }
    for(int i = 1; move.X  - 'a' - i >= 0 && move.Y + i < 8; ++i)
    {
        if(state_[move.X - 'a' - i][move.Y + i] == turn_) {
            for(int j = 1; j < i; ++j)
                state_[move.X - 'a' - j][move.Y+ j] = turn_;
            break;
        }
        if(state_[move.X - 'a' - i][move.Y + i] == -1)
            break;
    }
    for(int i = 1; move.X  - 'a' + i < 8 && move.Y - i >= 0; ++i)
    {
        if(state_[move.X - 'a' + i][move.Y - i] == turn_) {
            for(int j = 1; j < i; ++j)
                state_[move.X - 'a' + j][move.Y - j] = turn_;
            break;
        }
        if(state_[move.X - 'a' + i][move.Y - i] == -1)
            break;
    }
    for(int i = 1; move.X  - 'a' + i < 8 && move.Y + i < 8; ++i)
    {
        if(state_[move.X - 'a' + i][move.Y + i] == turn_) {
            for(int j = 1; j < i; ++j)
                state_[move.X - 'a' + j][move.Y + j] = turn_;
            break;
        }
        if(state_[move.X - 'a' + i][move.Y + i] == -1)
            break;
    }
    turn_ = (Color)((turn_ + 1) % 2);
}
int ReversiState::Get(Color color)
{
    int counter = 0;
    for(int i = 0; i < 8; ++i)
        for(int j = 0; j < 8; ++j)
        {
            if(state_[i][j] == color)
                counter += potentials_[i][j] + 1;
            else if(state_[i][j] != -1)
                counter -= potentials_[i][j];
        }
    return counter;
}

Color ReversiState::GetTurn()
{
    return turn_;
}
void ReversiState::SwapTurn()
{
    this->turn_ = (Color)((turn_ + 1) % 2);
}
std::vector<std::pair<Move,ReversiState>> ReversiState::GetPossibleMoves()
{
    auto oppositeTurn = ((turn_ + 1) % 2);
    std::vector<std::pair<Move,ReversiState>> moves;
    for(int i = 0; i < 8; ++i)
        for(int j = 0; j < 8; ++j)
            if(state_[i][j] == -1) {
                bool possibleMove = false;
                if(!possibleMove && i - 1 >= 0 && state_[i-1][j] == oppositeTurn)
                    for(int k = 2; k < 8; ++k)
                    {
                        if(i - k >= 0){
                            if(state_[i-k][j] == -1)
                                break;
                            if(state_[i-k][j] == turn_){
                                possibleMove = true;
                                break;
                            }
                        }
                    }
                 if(!possibleMove && j - 1 >= 0 && state_[i][j-1] == oppositeTurn)
                        for(int k = 2; k < 8; ++k)
                        {
                            if(j - k >= 0){
                                if(state_[i][j-k] == -1)
                                    break;
                                if(state_[i][j-k] == turn_){
                                    possibleMove = true;
                                    break;
                                }
                            }
                        }
                 if(!possibleMove && i + 1 < 8 && state_[i+1][j] == oppositeTurn)
                     for(int k = 2; k < 8; ++k)
                     {
                         if(i + k < 8){
                             if(state_[i+k][j] == -1)
                                 break;
                             if(state_[i+k][j] == turn_){
                                 possibleMove = true;
                                 break;
                             }
                         }
                     }
                 if(!possibleMove && j + 1 < 8 && state_[i][j+1] == oppositeTurn)
                     for(int k = 2; k < 8; ++k)
                     {
                         if(j + k < 8){
                             if(state_[i][j+k] == -1)
                                 break;
                             if(state_[i][j+k] == turn_){
                                 possibleMove = true;
                                 break;
                             }
                         }
                     }
                if(!possibleMove && i - 1 >= 0 && j - 1 >= 0 && state_[i-1][j-1] == oppositeTurn)
                    for(int k = 2; k < 8; ++k)
                    {
                        if(i - k >= 0 && j - k >= 0){
                            if(state_[i-k][j-k] == -1)
                                break;
                            if(state_[i-k][j-k] == turn_){
                                possibleMove = true;
                                break;
                            }
                        }
                    }
                if(!possibleMove && i - 1 >= 0 && j + 1 < 8 && state_[i-1][j+1] == oppositeTurn)
                    for(int k = 2; k < 8; ++k)
                    {
                        if(i - k >= 0 && j + k < 8){
                            if(state_[i-k][j+k] == -1)
                                break;
                            if(state_[i-k][j+k] == turn_){
                                possibleMove = true;
                                break;
                            }
                        }
                    }
                if(!possibleMove && i + 1 < 8 && j - 1 >= 0 && state_[i+1][j-1] == oppositeTurn)
                    for(int k = 2; k < 8; ++k)
                    {
                        if(i + k < 8 && j - k >= 0){
                            if(state_[i+k][j-k] == -1)
                                break;
                            if(state_[i+k][j-k] == turn_){
                                possibleMove = true;
                                break;
                            }
                        }
                    }
                if(!possibleMove && i + 1 < 8 && j + 1 < 8 && state_[i+1][j+1] == oppositeTurn)
                    for(int k = 2; k < 8; ++k)
                    {
                        if(i + k < 8 && j + k < 8){
                            if(state_[i+k][j+k] == -1)
                                break;
                            if(state_[i+k][j+k] == turn_){
                                possibleMove = true;
                                break;
                            }
                        }
                    }
                if(possibleMove)
                    moves.push_back(std::pair<Move, ReversiState>(Move{(char)i + 'a', j},
                                                                  ReversiState(*this, (Color)turn_, Move{ (char)i + 'a', j})));
            }
    return moves;
}
