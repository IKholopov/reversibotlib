#ifndef LOGGER_H
#define LOGGER_H

#include <istream>
#include <fstream>
#include <string>

class Logger
{
    public:
        static Logger& Log();

        void Write(std::string string)
        {
            file << string;
            file <<std::flush;
        }

    private:
        Logger();
        ~Logger();
        std::ofstream file;
};


#endif // LOGGER_H
