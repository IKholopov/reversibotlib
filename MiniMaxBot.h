#ifndef MINIMAXBOT_H
#define MINIMAXBOT_H

#include "IBot.h"
#include "ReversiState.h"
#include "StateTree.h"

class MiniMaxBot : public IBot
{
    public:
        MiniMaxBot();

        // IBot interface
        void Initialize(Color color, double timer);
        void OpponentMove(Move move);
        Move GetMove();
    private:
        ReversiState state_;
        Color color_;
        double timer_;
        StateTree tree_;
};

#endif // MINIMAXBOT_H
