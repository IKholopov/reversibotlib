#include "MiniMaxBot.h"

#include "Logger.h"
#include <sstream>
#include <thread>

MiniMaxBot::MiniMaxBot():tree_(Black)
{

}

void MiniMaxBot::Initialize(Color color, double timer)
{
    this->color_ = color;
    this->timer_ = timer;
    this->state_ = ReversiState();
    this->tree_.Initialize(color);
#ifdef DEBUG
    std::stringstream stream;
    //stream << "Initialized " << color << std::endl;
    Logger::Log().Write(stream.str());
#endif
}
void MiniMaxBot::OpponentMove(Move move)
{
    this->state_.ApplyMove(move);
    this->tree_.OpponentMove(move);
#ifdef DEBUG
    std::stringstream stream;
    stream << "move "  << move.X << " " << move.Y + 1 << "\n";
    Logger::Log().Write(stream.str());
    for(int i = 0; i < 8; ++i)
    {
        for(int j = 0; j < 8; ++j)
        {
            Logger::Log().Write(std::to_string(state_.state_[j][i]));
            Logger::Log().Write(" ");
        }
        Logger::Log().Write("\n");
    }
#endif

}
Move MiniMaxBot::GetMove()
{
    auto move = this->tree_.GetMove(timer_);
    this->state_.ApplyMove(move);
#ifdef DEBUG
    std::stringstream stream;
    stream << "turn" << "\n";
    stream << "move "  << move.X << " " << move.Y  + 1 << "\n";
    Logger::Log().Write(stream.str());
    for(int i = 0; i < 8; ++i)
    {
        for(int j = 0; j < 8; ++j)
        {
            //Logger::Log().Write(std::to_string(state_.state_[j][i]));
            //Logger::Log().Write(" ");
        }
        //Logger::Log().Write("\n");
    }
#endif
    return move;
}
