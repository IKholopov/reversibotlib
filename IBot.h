#ifndef IBOT_H
#define IBOT_H

#include "ReversiState.h"

class  IBot
{

    public:
        virtual void Initialize(Color color, double timer) = 0;
        virtual void OpponentMove(Move move) = 0;
        virtual Move GetMove() = 0;

};

#endif // IBOT_H
