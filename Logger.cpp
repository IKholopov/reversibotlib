#include "Logger.h"
#include <ctime>
#include <sstream>
#include <cstdlib>

Logger&Logger::Log()
{
    static Logger log;
    return log;
}

Logger::Logger()
{
    time_t t = time(0);
    struct tm * now = localtime( & t );
    std::stringstream filename;
    filename << "log" << t;
    file.open(filename.str(), std::ios_base::app);
    file << "Log started ";
        file << (now->tm_year + 1900) << '-'
             << (now->tm_mon + 1) << '-'
             <<  now->tm_mday
              << std::endl;
}

Logger::~Logger()
{
    file.close();
}

