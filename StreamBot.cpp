#include "StreamBot.h"
#include <istream>
#include <string>

StreamBot::StreamBot(std::unique_ptr<IBot> bot, std::istream& input, std::ostream& output, double timer):bot_(std::move(bot)),
        input_(input), output_(output), timer_(timer)
{
}

void StreamBot::Run()
{
    while(true)
    {
        std::string command;

        input_ >> command;
        if(command.compare("init") == 0) {
            input_ >> command;
            if(command.compare("black"))
                bot_->Initialize(Color::Black, timer_);
            else
                bot_->Initialize(Color::White, timer_);
            continue;
        }
        if(command.compare("move") == 0) {
            char x;
            int y;
            input_ >> x >> y;
            bot_->OpponentMove(Move{x, y - 1});
            continue;
        }
        if(command.compare("turn") == 0) {
            auto move = bot_->GetMove();
            output_ << "move " << move.X << " " << move.Y + 1 << std::endl;
            continue;
        }
        if(command.compare("win") == 0 || command.compare("lose") == 0 || command.compare("draw") == 0)
            break;
    }
}

