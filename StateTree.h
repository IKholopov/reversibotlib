#ifndef STATETREE_H
#define STATETREE_H

#include "ReversiState.h"
#include <unordered_map>
#include <memory>
#include <mutex>

class StateTree
{
    public:
        class Node{
            public:
                Node(ReversiState state, int depth, Node* parent = nullptr):Parent(parent),
                                                                            State(state), Score(0.0), Depth(depth), Expanded(false) {};
                Node* Parent;
                ReversiState State;
                double Score;
                int Depth;
                void Expand();
                std::unordered_map<Move, std::unique_ptr<Node>> Passes;
                bool Expanded;
        };
        StateTree(Color color);

        void Initialize(Color color);
        void ProceedWithMove(Move move);
        void OpponentMove(Move move);
        Move GetMove(double timer);
        void UpdateScores(std::vector<Node*>& updateNodes);
    private:
        std::unique_ptr<Node> root_;
        Color color_;
        std::mutex treeMutex;
};
#endif // STATETREE_H
